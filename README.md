# CO2-FootPrint

- This repository estimates my personal CO2 footprint in the 21st century
- It includes data on private car mileage, household energy consumption and air travel
- Data not available include train journeys, car rentals and business flights
- Data not considered include the CO2 footprint for production, disposal and recycling of considered items
- Items not considered include food and consumer goods 

## Private vehicles CO2 footprint and Mileage (2 Persons)

![Mileage-CO2](./Mileage/results/Mileage-CO2.png)

## Private household energy CO2 footprint (2 Persons)

![Household-CO2](./Household/results/Household-CO2.png)

## Air Travel CO2 emissions (single Person, both directions)

![AirTravel-CO2](./AirTravel/results/AirTravel-CO2.png)